#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

# python -m unittest Iota2TestsVectorTools

import os
import shutil
import sqlite3
import sys
import unittest

import pandas as pd

from iota2.simplification import vector_generalize as vg
from iota2.vector_tools import add_field_perimeter as afp
from iota2.vector_tools import buffer_ogr as bfo
from iota2.vector_tools import change_name_field as cnf
from iota2.vector_tools import check_geometry_area_thresh_field as check
from iota2.vector_tools import conditional_field_recode as cfr
from iota2.vector_tools import spatial_operations as so
from iota2.vector_tools import vector_functions as vf

IOTA2DIR = os.environ.get('IOTA2DIR')

if IOTA2DIR is None:
    raise Exception("IOTA2DIR environment variable must be set")

# if all tests pass, remove 'iota2_tests_directory' which contains all
# sub-directory tests
RM_IF_ALL_OK = True


class iota_testVectortools(unittest.TestCase):
    # before launching tests
    @classmethod
    def setUpClass(cls):
        # definition of local variables
        cls.group_test_name = "iota_testVectortools"
        cls.iota2_tests_directory = os.path.join(IOTA2DIR, "data",
                                                 cls.group_test_name)
        cls.all_tests_ok = []

        # Tests directory
        cls.test_working_directory = None
        if os.path.exists(cls.iota2_tests_directory):
            shutil.rmtree(cls.iota2_tests_directory)
        os.mkdir(cls.iota2_tests_directory)

        cls.wd = os.path.join(cls.iota2_tests_directory, "wd/")
        cls.out = os.path.join(cls.iota2_tests_directory, "out/")
        cls.classif = os.path.join(IOTA2DIR, "data",
                                   "references/vectortools/classif.shp")
        cls.inter = os.path.join(IOTA2DIR, "data",
                                 "references/vectortools/region.shp")
        cls.classifwd = os.path.join(cls.out, "classif.shp")
        cls.classifout = os.path.join(IOTA2DIR, "data",
                                      "references/vectortools/classifout.shp")
        cls.outinter = os.path.join(cls.wd, "inter.shp")
        cls.fake_region = os.path.join(IOTA2DIR, "data", "references",
                                       "running_iota2", "fake_region.shp")
        cls.classif_raster_file = os.path.join(IOTA2DIR, "data", "references",
                                               "Runs_standards",
                                               "Usual_s2_theia_run",
                                               "test_results", "final",
                                               "Classif_Seed_0.tif")
        cls.classif_vector_file = os.path.join(IOTA2DIR, "data", "references",
                                               "vectortools",
                                               "i2_classif.sqlite")

    # after launching all tests
    @classmethod
    def tearDownClass(cls):
        print("{} ended".format(cls.group_test_name))
        if RM_IF_ALL_OK and all(cls.all_tests_ok):
            shutil.rmtree(cls.iota2_tests_directory)

    # before launching a test
    def setUp(self):
        """
        create test environement (directories)
        """
        # self.test_working_directory is the diretory dedicated to each tests
        # it changes for each tests

        test_name = self.id().split(".")[-1]
        self.test_working_directory = os.path.join(self.iota2_tests_directory,
                                                   test_name)
        if os.path.exists(self.test_working_directory):
            shutil.rmtree(self.test_working_directory)
        os.mkdir(self.test_working_directory)

        if os.path.exists(self.wd):
            shutil.rmtree(self.wd, ignore_errors=True)
            os.mkdir(self.wd)
        else:
            os.mkdir(self.wd)

        if os.path.exists(self.out):
            shutil.rmtree(self.out, ignore_errors=True)
            os.mkdir(self.out)
        else:
            os.mkdir(self.out)

    def list2reason(self, exc_list):
        if exc_list and exc_list[-1][0] is self:
            return exc_list[-1][1]

    # after launching a test, remove test's data if test succeed
    def tearDown(self):
        if sys.version_info > (3, 4, 0):
            result = self.defaultTestResult()
            self._feedErrorsToResult(result, self._outcome.errors)
        else:
            result = getattr(self, '_outcomeForDoCleanups',
                             self._resultForDoCleanups)
        error = self.list2reason(result.errors)
        failure = self.list2reason(result.failures)
        ok = not error and not failure

        self.all_tests_ok.append(ok)
        if ok:
            shutil.rmtree(self.test_working_directory)

    # Tests definitions
    def test_iota2_vectortools(self):
        """Test how many samples must be add to the sample set
        """

        # Add Field
        for ext in ['.shp', '.dbf', '.shx', '.prj']:
            shutil.copyfile(
                os.path.splitext(self.classif)[0] + ext,
                os.path.splitext(self.classifwd)[0] + ext)

        afp.addFieldPerimeter(self.classifwd)
        tmpbuff = os.path.join(self.wd, "tmpbuff.shp")
        bfo.bufferPoly(self.classifwd, tmpbuff, -10)
        for ext in ['.shp', '.dbf', '.shx', '.prj']:
            shutil.copyfile(
                os.path.splitext(tmpbuff)[0] + ext,
                os.path.splitext(self.classifwd)[0] + ext)

        cnf.changeName(self.classifwd, "Classe", "class")
        self.assertEqual(vf.getNbFeat(self.classifwd), 144,
                         "Number of features does not fit")
        self.assertEqual(vf.getFields(self.classifwd), [
            'Validmean', 'Validstd', 'Confidence', 'Hiver', 'Ete', 'Feuillus',
            'Coniferes', 'Pelouse', 'Landes', 'UrbainDens', 'UrbainDiff',
            'ZoneIndCom', 'Route', 'PlageDune', 'SurfMin', 'Eau', 'GlaceNeige',
            'Prairie', 'Vergers', 'Vignes', 'Perimeter', 'class'
        ], "List of fields does not fit")
        self.assertEqual(
            vf.ListValueFields(self.classifwd, "class"),
            ['11', '12', '211', '222', '31', '32', '36', '42', '43', '51'],
            "Values of field 'class' do not fit")
        self.assertEqual(
            vf.getFieldType(self.classifwd, "class"), str,
            "Type of field 'class' (%s) do not fit, 'str' expected" %
            (vf.getFieldType(self.classifwd, "class")))

        cfr.conFieldRecode(self.classifwd, "class", "mask", 11, 0)
        so.intersectSqlites(self.classifwd, self.inter, self.wd, self.outinter,
                            2154, "intersection", [
                                'class', 'Validmean', 'Validstd', 'Confidence',
                                'ID', 'Perimeter', 'Aire', "mask"
                            ])
        check.check_geometry_area_thresh_field(self.outinter, 100, 1,
                                               self.classifwd)
        self.assertEqual(vf.getNbFeat(self.classifwd), 102,
                         "Number of features does not fit")

    def test_topological_polygonize(self):
        """test vector_generalize.topological_polygonize function behaviour
        """
        _, in_raster_name = os.path.split(self.classif_raster_file)
        out_vector = os.path.join(self.test_working_directory,
                                  in_raster_name.replace(".tif", ".sqlite"))
        if os.path.exists(out_vector):
            os.remove(out_vector)
        vg.topological_polygonize(
            self.test_working_directory,
            self.classif_raster_file,
            angle=True,
            out=out_vector,
            outformat="SQLite",
            debulvl="info",
            epsg="2154",
            working_dir=None,
            clipfile=self.fake_region,
            clipfield="region",
            clipvalue=1,
            bufferclip="20000",
        )
        con = sqlite3.connect(out_vector)
        df = pd.read_sql_query("SELECT * from vectile", con)
        self.assertTrue(
            list(df.columns) == ['ogc_fid', 'GEOMETRY', 'cat', 'label'])
        self.assertTrue(len(df) >= 1)

    def test_generalization(self):
        """test vector_generalize.generalize_vector function behaviour
        """
        _, in_vector_name = os.path.split(self.classif_vector_file)

        out_vector_file = os.path.join(
            self.test_working_directory,
            in_vector_name.replace(".sqlite", "_douglas.sqlite"))

        if os.path.exists(out_vector_file):
            os.remove(out_vector_file)
        vg.generalize_vector(self.test_working_directory,
                             self.classif_vector_file,
                             paramgene=10,
                             method="douglas",
                             mmu="",
                             ncolumns="cat",
                             out=out_vector_file,
                             outformat="SQLite",
                             debulvl="info",
                             epsg="2154",
                             working_dir=None,
                             clipfile=None,
                             snap=1)
        print(out_vector_file)
        con = sqlite3.connect(out_vector_file)
        df = pd.read_sql_query("SELECT * from tmp", con)
        self.assertTrue(
            list(df.columns) == ['ogc_fid', 'GEOMETRY', 'cat', 'area', 'id'])
        self.assertTrue(len(df) >= 1)
