#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import argparse
import logging
import os
from typing import Dict, Optional

import numpy as np

from iota2.common import file_utils as fu

LOGGER = logging.getLogger("distributed.worker")


def genResults(pathRes: str, pathNom: str, labels_conversion_table: Dict):
    """
    generate IOTA² final report
    """
    from iota2.validation import results_utils as resU

    # get a copy for concatenate_sample_selection
    i2_labels_to_user_labels = labels_conversion_table.copy()

    lab_conv_tab: Optional[Dict] = labels_conversion_table
    all_castable = []
    for _, user_label in labels_conversion_table.items():
        try:
            _ = int(user_label)
            all_castable.append(True)
        except ValueError:
            all_castable.append(False)
    re_encode_labels = all(all_castable)
    if re_encode_labels:
        lab_conv_tab = None

    all_csv = fu.file_search_and(pathRes + "/TMP", True, "Classif", ".csv")

    resU.stats_report(all_csv,
                      pathNom,
                      os.path.join(pathRes, "RESULTS.txt"),
                      labels_table=lab_conv_tab)

    for seed_csv in all_csv:
        name, ext = os.path.splitext(os.path.basename(seed_csv))
        out_png = os.path.join(pathRes, "Confusion_Matrix_{}.png".format(name))
        resU.gen_confusion_matrix_fig(seed_csv,
                                      out_png,
                                      pathNom,
                                      undecidedlabel=None,
                                      dpi=200,
                                      write_conf_score=True,
                                      grid_conf=True,
                                      conf_score="count_sci",
                                      threshold=0.1,
                                      labels_table=lab_conv_tab)

    # merge samples selection csv
    output_path = os.path.normpath(os.path.join(pathRes, "../"))  # FIXME
    resU.merge_all_sample_selection(output_path, pathNom,
                                    i2_labels_to_user_labels)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description=
        "This function shape classifications (fake fusion and tiles priority)")
    parser.add_argument(
        "-path.res",
        help=
        "path to the folder which contains classification's results (mandatory)",
        dest="pathRes",
        required=True)
    parser.add_argument("-path.nomenclature",
                        help="path to the nomenclature (mandatory)",
                        dest="pathNom",
                        required=True)
    args = parser.parse_args()

    genResults(args.pathRes, args.pathNom)
