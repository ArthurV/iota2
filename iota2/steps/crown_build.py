#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os

from iota2.configuration_files import read_config_file as rcf
from iota2.simplification import build_crown_raster as bcr
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class CrownBuild(iota2_step.Step):
    resources_block_name = "crownbuild"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        self.workingdirectory = workingDirectory
        self.outputpath = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.gridsize = rcf.read_config_file(self.cfg).getParam(
            'simplification', 'gridsize')
        self.blocksize = rcf.read_config_file(self.cfg).getParam(
            'simplification', 'blocksize')

        tmpdir = os.path.join(self.outputpath, "simplification", "tmp")
        outpathtile = os.path.join(self.outputpath, "simplification", "tiles")
        tiles_list = os.path.join(self.outputpath, "simplification", "tiles")

        for grid_tile in range(self.gridsize**2):
            task = self.i2_task(task_name=f"crown_build_{grid_tile}",
                                log_dir=self.log_step_dir,
                                execution_mode=self.execution_mode,
                                task_parameters={
                                    "f": bcr.manage_blocks,
                                    "crowns_path": tiles_list,
                                    "tilenumber": grid_tile,
                                    "blocksize": self.blocksize,
                                    "inpath": tmpdir,
                                    "outpath": outpathtile,
                                    "working_dir": self.workingdirectory,
                                },
                                task_resources=self.get_resources())
            self.add_task_to_i2_processing_graph(
                task,
                task_group="tile_grid",
                task_sub_group=f"tile_grid_{grid_tile}",
                task_dep_dico={"tile_grid": [f"tile_grid_{grid_tile}"]})

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Build crown raster for serialization process")
        return description
