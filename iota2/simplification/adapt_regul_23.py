#!/usr/bin/python
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Adaptive and majority voting regularization process based on landscape rules

"""

import argparse
import multiprocessing as mp
import os
import shutil
import sys
import time
from functools import partial

import numpy as np

try:
    from iota2.common import otb_app_bank, utils
    from iota2.common.otb_app_bank import executeApp

except ImportError:
    raise ImportError('Iota2 not well configured / installed')

#------------------------------------------------------------------------------


def regularisation(raster, threshold, nbcores, path, ram="128"):

    filetodelete = []

    # First regularisation in connection 8, second in connection 4
    init_regul = time.time()

    # A mask for each regularization rule
    # Agricultuture
    bandMathAppli = otb_app_bank.CreateBandMathApplication({
        "il":
        raster,
        "exp":
        '(im1b1==5 || im1b1==6 || im1b1==7 || im1b1==8 || im1b1==9 || im1b1==10 || im1b1==11 || im1b1==12)?im1b1:0',
        "ram":
        str(1 * float(ram)),
        "pixType":
        "uint8",
        "out":
        os.path.join(path, 'mask_1.tif')
    })
    p = mp.Process(target=executeApp, args=[bandMathAppli])
    p.start()
    p.join()
    #bandMathAppli.ExecuteAndWriteOutput()
    filetodelete.append(os.path.join(path, 'mask_1.tif'))

    # Forest
    bandMathAppli = otb_app_bank.CreateBandMathApplication({
        "il":
        raster,
        "exp":
        '(im1b1==16 || im1b1==17)?im1b1:0',
        "ram":
        str(1 * float(ram)),
        "pixType":
        "uint8",
        "out":
        os.path.join(path, 'mask_2.tif')
    })
    p = mp.Process(target=executeApp, args=[bandMathAppli])
    p.start()
    p.join()
    #bandMathAppli.ExecuteAndWriteOutput()
    filetodelete.append(os.path.join(path, 'mask_2.tif'))
    # Urban
    bandMathAppli = otb_app_bank.CreateBandMathApplication({
        "il":
        raster,
        "exp":
        '(im1b1==1 || im1b1==2 || im1b1==3)?im1b1:0',
        "ram":
        str(1 * float(ram)),
        "pixType":
        "uint8",
        "out":
        os.path.join(path, 'mask_3.tif')
    })
    p = mp.Process(target=executeApp, args=[bandMathAppli])
    p.start()
    p.join()
    #bandMathAppli.ExecuteAndWriteOutput()
    filetodelete.append(os.path.join(path, 'mask_3.tif'))
    # Open natural areas
    bandMathAppli = otb_app_bank.CreateBandMathApplication({
        "il":
        raster,
        "exp":
        '(im1b1==18 || im1b1==19 || im1b1==13)?im1b1:0',
        "ram":
        str(1 * float(ram)),
        "pixType":
        "uint8",
        "out":
        os.path.join(path, 'mask_4.tif')
    })
    p = mp.Process(target=executeApp, args=[bandMathAppli])
    p.start()
    p.join()
    #bandMathAppli.ExecuteAndWriteOutput()
    filetodelete.append(os.path.join(path, 'mask_4.tif'))
    # Bare soil
    bandMathAppli = otb_app_bank.CreateBandMathApplication({
        "il":
        raster,
        "exp":
        '(im1b1==20 || im1b1==21)?im1b1:0',
        "ram":
        str(1 * float(ram)),
        "pixType":
        "uint8",
        "out":
        os.path.join(path, 'mask_5.tif')
    })
    p = mp.Process(target=executeApp, args=[bandMathAppli])
    p.start()
    p.join()
    #bandMathAppli.ExecuteAndWriteOutput()
    filetodelete.append(os.path.join(path, 'mask_5.tif'))
    # Perennial agriculture
    bandMathAppli = otb_app_bank.CreateBandMathApplication({
        "il":
        raster,
        "exp":
        '(im1b1==14 || im1b1==15)?im1b1:0',
        "ram":
        str(1 * float(ram)),
        "pixType":
        "uint8",
        "out":
        os.path.join(path, 'mask_6.tif')
    })
    p = mp.Process(target=executeApp, args=[bandMathAppli])
    p.start()
    p.join()
    #bandMathAppli.ExecuteAndWriteOutput()
    filetodelete.append(os.path.join(path, 'mask_6.tif'))
    # Road
    bandMathAppli = otb_app_bank.CreateBandMathApplication({
        "il":
        raster,
        "exp":
        '(im1b1==4)?im1b1:0',
        "ram":
        str(1 * float(ram)),
        "pixType":
        "uint8",
        "out":
        os.path.join(path, 'mask_7.tif')
    })
    p = mp.Process(target=executeApp, args=[bandMathAppli])
    p.start()
    p.join()
    #bandMathAppli.ExecuteAndWriteOutput()
    filetodelete.append(os.path.join(path, 'mask_7.tif'))
    # Water
    bandMathAppli = otb_app_bank.CreateBandMathApplication({
        "il":
        raster,
        "exp":
        '(im1b1==23)?im1b1:0',
        "ram":
        str(1 * float(ram)),
        "pixType":
        "uint8",
        "out":
        os.path.join(path, 'mask_8.tif')
    })
    p = mp.Process(target=executeApp, args=[bandMathAppli])
    p.start()
    p.join()
    #bandMathAppli.ExecuteAndWriteOutput()
    filetodelete.append(os.path.join(path, 'mask_8.tif'))
    # Snow and glacier
    bandMathAppli = otb_app_bank.CreateBandMathApplication({
        "il":
        raster,
        "exp":
        '(im1b1==22)?im1b1:0',
        "ram":
        str(1 * float(ram)),
        "pixType":
        "uint8",
        "out":
        os.path.join(path, 'mask_9.tif')
    })
    p = mp.Process(target=executeApp, args=[bandMathAppli])
    p.start()
    p.join()
    #bandMathAppli.ExecuteAndWriteOutput()
    filetodelete.append(os.path.join(path, 'mask_9.tif'))

    for i in range(9):
        command = "gdalwarp -q -multi -wo NUM_THREADS=%s -dstnodata 0 %s/mask_%s.tif %s/mask_nd_%s.tif"%(nbcores, \
                                                                                                         path, \
                                                                                                         str(i + 1), \
                                                                                                         path, \
                                                                                                         str(i + 1))
        utils.run(command)
        filetodelete.append("%s/mask_nd_%s.tif" % (path, str(i + 1)))

    masktime = time.time()
    print(" ".join([
        " : ".join([
            "Masks generation for adaptive rules",
            str(masktime - init_regul)
        ]), "seconds"
    ]))

    # Two successive regularisation (8 neighbors then 4 neighbors)
    for i in range(2):

        if i == 0:
            connexion = 8
        else:
            connexion = 4

        # Tiles number to treat in parralel
        pool = mp.Pool(processes=6)
        iterable = (np.arange(6)).tolist()
        function = partial(gdal_sieve, threshold, connexion, path)
        pool.map(function, iterable)
        pool.close()
        pool.join()

        for j in range(6):
            command = "gdalwarp -q -multi -wo NUM_THREADS=%s -dstnodata 0 %s/mask_%s_%s.tif %s/mask_nd_%s_%s.tif"%(nbcores, \
                                                                                                                path, \
                                                                                                                str(j + 1), \
                                                                                                                str(connexion), \
                                                                                                                path, \
                                                                                                                str(j + 1), \
                                                                                                                str(connexion))
            utils.run(command)

        for j in range(6):
            os.remove(path + "/mask_%s_%s.tif" % (str(j + 1), str(connexion)))

    for j in range(6):
        os.remove(path + "/mask_nd_%s_8.tif" % (str(j + 1)))

    adaptivetime = time.time()
    print(" ".join([
        " : ".join(["Adaptive regularizations",
                    str(adaptivetime - masktime)]), "seconds"
    ]))

    # Fusion of rule-based regularisation
    rastersList = [os.path.join(path, "mask_nd_1_4.tif"), os.path.join(path, "mask_nd_2_4.tif"), os.path.join(path, "mask_nd_3_4.tif"), \
                   os.path.join(path, "mask_nd_4_4.tif"), os.path.join(path, "mask_nd_5_4.tif"), os.path.join(path, "mask_nd_6_4.tif"), \
                   os.path.join(path, "mask_nd_7.tif"), os.path.join(path, "mask_nd_8.tif"), os.path.join(path, "mask_nd_9.tif")]

    bandMathAppli = otb_app_bank.CreateBandMathApplication({
        "il":
        rastersList,
        "exp":
        'im1b1+im2b1+\
                                                                im3b1+im4b1+\
                                                                im5b1+im6b1+\
                                                                im7b1+im8b1+\
                                                                im9b1',
        "ram":
        str(1 * float(ram)),
        "pixType":
        "uint8",
        "out":
        os.path.join(path, 'mask_regul_adapt.tif')
    })
    p = mp.Process(target=executeApp, args=[bandMathAppli])
    p.start()
    p.join()
    #bandMathAppli.ExecuteAndWriteOutput()

    for filemask in rastersList:
        os.remove(filemask)

    command = "gdalwarp -q -multi -wo NUM_THREADS="
    command += "%s -dstnodata 0 %s/mask_regul_adapt.tif %s/mask_nd_regul_adapt.tif"%(nbcores, \
                                                                                     path, \
                                                                                     path)
    utils.run(command)
    filetodelete.append("%s/mask_regul_adapt.tif" % (path))

    # Regularisation based on majority voting

    # 8 neighbors
    command = "gdal_sieve.py -q -8 -st "
    command += "%s %s/mask_nd_regul_adapt.tif %s/mask_regul_adapt_0.tif" %(threshold, \
                                                                           path, \
                                                                           path)
    utils.run(command)
    filetodelete.append("%s/mask_nd_regul_adapt.tif" % (path))

    command = "gdalwarp -q -multi -wo NUM_THREADS="
    command += "%s -dstnodata 0 %s/mask_regul_adapt_0.tif %s/mask_nd_regul_adapt_0.tif"%(nbcores, \
                                                                                         path, \
                                                                                         path)
    utils.run(command)
    filetodelete.append("%s/mask_regul_adapt_0.tif" % (path))

    # 4 neighbors
    command = "gdal_sieve.py -q -4 -st "
    command += "%s %s/mask_nd_regul_adapt_0.tif %s/regul_adapt_maj.tif" %(threshold, \
                                                                          path, \
                                                                          path)
    utils.run(command)
    filetodelete.append("%s/mask_nd_regul_adapt_0.tif" % (path))

    out_classif_sieve = "%s/regul_adapt_maj.tif" % (path)

    majoritytime = time.time()
    print(" ".join([
        " : ".join([
            "Majority voting regularization",
            str(majoritytime - adaptivetime)
        ]), "seconds"
    ]))

    for filetodel in filetodelete:
        if os.path.exists(filetodel):
            os.remove(filetodel)

    end_regul = time.time() - init_regul

    return out_classif_sieve, end_regul


def gdal_sieve(threshold, connexion, path, i):

    if connexion == 8:
        command = "gdal_sieve.py -q -%s -st %s %s/mask_nd_%s.tif %s/mask_%s_8.tif" % (
            connexion, threshold, path, str(i + 1), path, str(i + 1))
        utils.run(command)

    else:
        command = "gdal_sieve.py -q -%s -st %s %s/mask_nd_%s_8.tif %s/mask_%s_4.tif" % (
            connexion, threshold, path, str(i + 1), path, str(i + 1))
        utils.run(command)


if __name__ == "__main__":
    if len(sys.argv) == 1:
        prog = os.path.basename(sys.argv[0])
        print('      ' + sys.argv[0] + ' [options]')
        print("     Help : ", prog, " --help")
        print("        or : ", prog, " -h")
        sys.exit(-1)
    else:
        usage = "usage: %prog [options] "
        parser = argparse.ArgumentParser(
            description=
            "Adaptive and majority voting regularization process based on landscape rules"
        )
        parser.add_argument("-wd", dest="path", action="store", \
                            help="Input path where classification is located", required = True)
        parser.add_argument("-in", dest="classif", action="store", \
                            help="Name of classification", required = True)
        parser.add_argument("-nbcore", dest="core", action="store", \
                            help="Number of cores to use for OTB applications", required = True)
        parser.add_argument("-mmu", dest="mmu", action="store", \
                            help="Minimal mapping unit (in input classificaiton raster file unit)", required = True)
        parser.add_argument("-ram", dest="ram", action="store", \
                            help="Ram for otb processes", required = True)

    args = parser.parse_args()
    os.environ["ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS"] = str(args.core)
    regularisation(args.classif, args.mmu, args.core, args.path, args.ram)
