#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import configparser
import logging
import os
import shutil
import sys
from functools import partial

from osgeo import osr
from osgeo.gdalconst import *

import iota2.common.i2_constants as i2_const
from iota2.common import file_utils as fut
from iota2.common import otb_app_bank
from iota2.sensors.SAR import S1FilteringProcessor

from . import S1FileManager as s1_manager

logger = logging.getLogger("distributed.worker")
I2_CONST = i2_const.iota2_constants()


def get_orbit_direction(manifest):
    with open(manifest, "r") as saveFile:
        for line in saveFile:
            if "<s1:pass>" in line:
                if "DESCENDING" in line:
                    return "DES"
                if "ASCENDING" in line:
                    return "ASC"
        return ""
        raise Exception("Orbit Directiction not found in " + str(manifest))


def get_platform_from_s1_raster(PathToRaster):
    return PathToRaster.split("/")[-1].split("-")[0]


class Sentinel1PreProcess(object):
    def __init__(self, configFile):

        config = configparser.ConfigParser()
        config.read(configFile)
        try:
            os.remove("S1ProcessorErr.log.log")
            os.remove("S1ProcessorOut.log")
        except:
            pass
        self.raw_directory = config.get('Paths', 'S1Images')
        self.VH_pattern = "measurement/*vh*-???.tiff"
        self.VV_pattern = "measurement/*vv*-???.tiff"
        self.manifest_pattern = "manifest.safe"
        self.outputPreProcess = config.get('Paths', 'Output')
        self.SRTM = config.get('Paths', 'SRTM')
        self.geoid = config.get('Paths', 'GeoidFile')

        self.borderThreshold = float(
            config.get('Processing',
                       'BorderThreshold',
                       fallback=I2_CONST.s1_border_threshold))
        self.RAMPerProcess = int(
            config.get('Processing',
                       'RAMPerProcess',
                       fallback=I2_CONST.s1_ram_per_process))

        self.tilesList = [
            s.strip() for s in config.get('Processing', 'Tiles').split(",")
        ]

        self.referencesFolder = config.get('Processing', 'ReferencesFolder')
        self.rasterPattern = config.get('Processing', 'RasterPattern')


def SAR_floatToInt(filterApplication,
                   nb_bands,
                   RAMPerProcess,
                   outputFormat="uint16",
                   db_min=-25,
                   db_max=3):
    """transform float SAR values to integer
    """

    min_val = str(10.0**(db_min / 10.0))
    max_val = str(10.0**(db_max / 10.0))

    min_val_scale = 0
    max_val_scale = "((2^16)-1)"
    if outputFormat == "uint8":
        max_val_scale = "((2^8)-1)"

    #build expression
    to_db_expression = "10*log10(im1bX)"
    scale_expression = "(({}-{})/({}-{}))*({})+({}-(({}-{})*{})/({}-{}))".format(
        max_val_scale, min_val_scale, db_max, db_min, to_db_expression,
        min_val_scale, max_val_scale, min_val_scale, db_min, db_max, db_min)
    scale_max_val = (2**16) - 1
    scale_min_val = 0
    threshold_expression = "{0}>{1}?{3}:{0}<{2}?{4}:{5}".format(
        to_db_expression, db_max, db_min, scale_max_val, scale_min_val,
        scale_expression)

    expression = ";".join([
        threshold_expression.replace("X", str(i + 1)) for i in range(nb_bands)
    ])

    outputPath = filterApplication.GetParameterValue(
        otb_app_bank.getInputParameterOutput(filterApplication))

    convert = otb_app_bank.CreateBandMathXApplication({
        "il": filterApplication,
        "out": outputPath,
        "exp": expression,
        "ram": str(RAMPerProcess),
        "pixType": outputFormat
    })
    return convert


def writeOutputRaster(OTB_App,
                      overwrite=True,
                      workingDirectory=None,
                      logger=logger):
    """
    """
    out_param = otb_app_bank.getInputParameterOutput(OTB_App)
    out_raster = OTB_App.GetParameterValue(out_param)

    launch_write = True
    if os.path.exists(out_raster.split("?")[0]) and not overwrite:
        launch_write = False

    if workingDirectory is None and launch_write:
        OTB_App.ExecuteAndWriteOutput()

    elif launch_write:
        out_raster_dir, out_raster_name = os.path.split(out_raster)
        out_workingDir = os.path.join(workingDirectory, out_raster_name)
        out_workingDir = out_workingDir.split("?")[0]
        OTB_App.SetParameterString(out_param, out_workingDir)
        OTB_App.ExecuteAndWriteOutput()
        shutil.copy(out_workingDir, out_raster.split("?")[0])
        if os.path.exists(out_workingDir.replace(".tif", ".geom")):
            shutil.copy(out_workingDir.replace(".tif", ".geom"),
                        out_raster.replace(".tif", ".geom").split("?")[0])
    if not launch_write:
        logger.info(
            "{} already exists and will not be overwrited".format(out_raster))

    OTB_App = None
    return out_raster


def generate_border_mask(data_img, out_mask, RAMPerProcess=4000):
    """
    """

    threshold = 0.0011
    mask = otb_app_bank.CreateBandMathApplication({
        "il":
        data_img,
        "exp":
        "im1b1<{}?1:0".format(threshold),
        "ram":
        str(RAMPerProcess),
        "pixType":
        'uint8'
    })
    mask.Execute()
    borderMask = otb_app_bank.CreateBinaryMorphologicalOperation({
        "in":
        mask,
        "out":
        out_mask,
        "ram":
        str(RAMPerProcess),
        "pixType":
        "uint8",
        "filter":
        "opening",
        "ballxradius":
        5,
        "ballyradius":
        5
    })
    dep = mask
    return borderMask, dep


def launch_sar_reprojection(rasterList,
                            refRaster=None,
                            tileName=None,
                            SRTM=None,
                            geoid=None,
                            output_directory=None,
                            RAMPerProcess=None,
                            workingDirectory=None):
    """must be use with multiprocessing.Pool
    """
    def writeOutputRaster_2(OTB_App,
                            overwrite=True,
                            workingDirectory=None,
                            dep=None,
                            logger=logger):
        """
        """
        out_param = otb_app_bank.getInputParameterOutput(OTB_App)
        out_raster = OTB_App.GetParameterValue(out_param)

        launch_write = True
        if os.path.exists(out_raster.split("?")[0]) and not overwrite:
            launch_write = False

        if workingDirectory is None and launch_write:
            OTB_App.ExecuteAndWriteOutput()

        elif launch_write:
            out_raster_dir, out_raster_name = os.path.split(out_raster)
            out_workingDir = os.path.join(workingDirectory, out_raster_name)
            out_workingDir = out_workingDir.split("?")[0]
            OTB_App.SetParameterString(out_param, out_workingDir)
            OTB_App.ExecuteAndWriteOutput()
            shutil.copy(out_workingDir, out_raster.split("?")[0])
            if os.path.exists(out_workingDir.replace(".tif", ".geom")):
                shutil.copy(out_workingDir.replace(".tif", ".geom"),
                            out_raster.replace(".tif", ".geom").split("?")[0])
        if not launch_write:
            logger.info("{} already exists and will not be overwrited".format(
                out_raster))

        OTB_App = None
        return out_raster

    date_position = 4

    all_superI_vv = []
    all_superI_vh = []
    all_acquisition_date_vv = []
    all_acquisition_date_vh = []
    dates = []
    all_dep = []
    for date_to_Concatenate in rasterList:
        #Calibration + Superimpose
        vv, vh = date_to_Concatenate.GetImageList()
        SAR_directory, SAR_name = os.path.split(vv)
        currentPlatform = get_platform_from_s1_raster(vv)
        manifest = date_to_Concatenate.getManifest()
        currentOrbitDirection = get_orbit_direction(manifest)
        acquisition_date = SAR_name.split("-")[date_position]
        dates.append(acquisition_date)
        calib_vv = otb_app_bank.CreateSarCalibration({
            "in": vv,
            "lut": "gamma",
            "ram": str(RAMPerProcess)
        })
        calib_vh = otb_app_bank.CreateSarCalibration({
            "in": vh,
            "lut": "gamma",
            "ram": str(RAMPerProcess)
        })
        calib_vv.Execute()
        calib_vh.Execute()
        all_dep.append(calib_vv)
        all_dep.append(calib_vh)
        orthoImageName_vv = "{}_{}_{}_{}_{}".format(currentPlatform, tileName,
                                                    "vv",
                                                    currentOrbitDirection,
                                                    acquisition_date)

        super_vv, super_vv_dep = otb_app_bank.CreateSuperimposeApplication({
            "inr":
            refRaster,
            "inm":
            calib_vv,
            "pixType":
            "float",
            "interpolator":
            "bco",
            "ram":
            RAMPerProcess,
            "elev.dem":
            SRTM,
            "elev.geoid":
            geoid
        })
        orthoImageName_vh = "{}_{}_{}_{}_{}".format(currentPlatform, tileName,
                                                    "vh",
                                                    currentOrbitDirection,
                                                    acquisition_date)

        super_vh, super_vh_dep = otb_app_bank.CreateSuperimposeApplication({
            "inr":
            refRaster,
            "inm":
            calib_vh,
            "pixType":
            "float",
            "interpolator":
            "bco",
            "ram":
            RAMPerProcess,
            "elev.dem":
            SRTM,
            "elev.geoid":
            geoid
        })
        super_vv.Execute()
        super_vh.Execute()
        all_dep.append(super_vv)
        all_dep.append(super_vh)
        all_superI_vv.append(super_vv)
        all_superI_vh.append(super_vh)

        all_acquisition_date_vv.append(orthoImageName_vv)
        all_acquisition_date_vh.append(orthoImageName_vh)

    all_acquisition_date_vv = "_".join(sorted(all_acquisition_date_vv))
    all_acquisition_date_vh = "_".join(sorted(all_acquisition_date_vh))
    #Concatenate thanks to a BandMath
    vv_exp = ",".join(
        ["im{}b1".format(i + 1) for i in range(len(all_superI_vv))])
    vv_exp = "max({})".format(vv_exp)
    SAR_vv = os.path.join(output_directory, all_acquisition_date_vv + ".tif")
    concatAppli_vv = otb_app_bank.CreateBandMathApplication({
        "il":
        all_superI_vv,
        "exp":
        vv_exp,
        "out":
        SAR_vv,
        "ram":
        str(RAMPerProcess),
        "pixType":
        "float"
    })
    vh_exp = ",".join(
        ["im{}b1".format(i + 1) for i in range(len(all_superI_vh))])
    vh_exp = "max({})".format(vh_exp)
    SAR_vh = os.path.join(output_directory, all_acquisition_date_vh + ".tif")
    concatAppli_vh = otb_app_bank.CreateBandMathApplication({
        "il":
        all_superI_vh,
        "exp":
        vh_exp,
        "out":
        SAR_vh,
        "ram":
        str(RAMPerProcess),
        "pixType":
        "float"
    })

    ortho_path = writeOutputRaster_2(concatAppli_vv,
                                     overwrite=False,
                                     workingDirectory=workingDirectory,
                                     dep=all_dep)
    ortho_path = writeOutputRaster_2(concatAppli_vh,
                                     overwrite=False,
                                     workingDirectory=workingDirectory,
                                     dep=all_dep)

    #from the results generate a mask
    super_vv = os.path.join(output_directory, all_acquisition_date_vv + ".tif")
    border_mask = super_vv.replace(".tif", "_BorderMask.tif")
    mask_app, _ = generate_border_mask(super_vv,
                                       border_mask,
                                       RAMPerProcess=RAMPerProcess)
    mask_path = writeOutputRaster(mask_app,
                                  overwrite=False,
                                  workingDirectory=workingDirectory)
    mask_path_geom = mask_path.replace(".tif", ".geom")
    if os.path.exists(mask_path_geom):
        os.remove(mask_path_geom)

    return (SAR_vv, SAR_vh)


def concatenate_dates(rasterList):
    """from a list of raster, find raster to concatenates (same acquisition date)
    """

    date_position = 4
    date_SAR = []
    for raster in rasterList:
        vv, vh = raster.imageFilenamesList
        SAR_directory, SAR_name = os.path.split(vv)
        acquisition_date = SAR_name.split("-")[date_position].split("t")[0]
        date_SAR.append((acquisition_date, raster))
    date_SAR = fut.sort_by_first_elem(date_SAR)
    return [
        tuple(listOfConcatenation) for date, listOfConcatenation in date_SAR
    ]


def split_by_mode(rasterList):
    """
    """
    modes = {"s1a": {"ASC": [], "DES": []}, "s1b": {"ASC": [], "DES": []}}
    for raster, coordinates in rasterList:
        manifest = raster.getManifest()
        currentOrbitDirection = get_orbit_direction(manifest)
        currentPlatform = get_platform_from_s1_raster(
            raster.imageFilenamesList[0])
        modes[currentPlatform][currentOrbitDirection].append(raster)
    return modes["s1a"]["ASC"], modes["s1a"]["DES"], modes["s1b"][
        "ASC"], modes["s1b"]["DES"]


def get_sar_dates(rasterList):
    """
    """
    date_position = 4
    dates = []
    for raster in rasterList:
        vv, vh = raster.imageFilenamesList
        SAR_directory, SAR_name = os.path.split(vv)
        acquisition_date = SAR_name.split("-")[date_position]
        dates.append(acquisition_date)
    dates = sorted(dates)
    return dates


def s1_preprocess(cfg, process_tile, workingDirectory=None, getFiltered=False):
    """
    IN
    cfg [string] : path to a configuration file
    process_tile [list] : list of tiles to be processed
    workingDirectory [string] : path to a working directory

    OUT [list of otb's applications need to filter SAR images]
        allFiltered,allDependence,allMasksOut,allTile
    """

    if process_tile and not isinstance(process_tile, list):
        process_tile = [process_tile]

    config = configparser.ConfigParser()
    config.read(cfg)
    RAMPerProcess = int(
        config.get('Processing',
                   'RAMPerProcess',
                   fallback=I2_CONST.s1_ram_per_process))
    S1chain = Sentinel1PreProcess(cfg)
    S1FileManager = s1_manager.S1FileManager(cfg)
    tilesToProcess = []

    convert_to_interger = False

    tilesToProcess = [cTile[1:] for cTile in process_tile]

    if len(tilesToProcess) == 0:
        print("No existing tiles found, exiting ...")
        sys.exit(1)

    # Analyse SRTM coverage for MGRS tiles to be processed
    srtm_tiles_check = S1FileManager.check_srtm_coverage(tilesToProcess)

    needed_srtm_tiles = []
    tilesToProcessChecked = []
    # For each MGRS tile to process
    for tile in tilesToProcess:
        # Get SRTM tiles coverage statistics
        srtm_tiles = srtm_tiles_check[tile]
        current_coverage = 0
        current_needed_srtm_tiles = []
        # Compute global coverage
        for (srtm_tile, coverage) in srtm_tiles:
            current_needed_srtm_tiles.append(srtm_tile)
            current_coverage += coverage
        # If SRTM coverage of MGRS tile is enough, process it
        if current_coverage >= 1.:
            needed_srtm_tiles += current_needed_srtm_tiles
            tilesToProcessChecked.append(tile)
        else:
            # Skip it
            print("Tile " + str(tile) + " has insuficient SRTM coverage (" +
                  str(100 * current_coverage) + "%), it will not be processed")

    # Remove duplicates
    needed_srtm_tiles = list(set(needed_srtm_tiles))

    if len(tilesToProcessChecked) == 0:
        print("No tiles to process, exiting ...")
        sys.exit(1)

    print("Required SRTM tiles: " + str(needed_srtm_tiles))

    srtm_ok = True

    for srtm_tile in needed_srtm_tiles:
        tile_path = os.path.join(S1chain.SRTM, srtm_tile)
        if not os.path.exists(tile_path):
            srtm_ok = False
            print(tile_path + " is missing")
    if not srtm_ok:
        print("Some SRTM tiles are missing, exiting ...")
        sys.exit(1)

    if not os.path.exists(S1chain.geoid):
        print("Geoid file does not exists (" + S1chain.geoid +
              "), exiting ...")
        sys.exit(1)

    tilesSet = list(tilesToProcessChecked)
    rasterList = [
        elem for elem, coordinates in S1FileManager.get_s1_intersect_by_tile(
            tilesSet[0])
    ]

    comp_per_date = 2  #VV / VH

    tile = tilesToProcessChecked[0]

    allMasks = []
    if workingDirectory:
        workingDirectory = os.path.join(workingDirectory, tile)
        if not os.path.exists(workingDirectory):
            try:
                os.mkdir(workingDirectory)
            except:
                pass

    refRaster = fut.file_search_and(S1chain.referencesFolder + "/T" + tile,
                                    True, S1chain.rasterPattern)[0]

    #get SAR rasters which intersection the tile
    rasterList = S1FileManager.get_s1_intersect_by_tile(tile)
    #split SAR rasters in different groups
    (rasterList_s1aASC, rasterList_s1aDES, rasterList_s1bASC,
     rasterList_s1bDES) = split_by_mode(rasterList)
    #get detected dates by acquisition mode
    s1_ASC_dates = get_sar_dates(rasterList_s1aASC + rasterList_s1bASC)
    s1_DES_dates = get_sar_dates(rasterList_s1aDES + rasterList_s1bDES)

    #find which one as to be concatenate (acquisitions dates are the same)
    rasterList_s1aASC = concatenate_dates(rasterList_s1aASC)
    rasterList_s1aDES = concatenate_dates(rasterList_s1aDES)
    rasterList_s1bASC = concatenate_dates(rasterList_s1bASC)
    rasterList_s1bDES = concatenate_dates(rasterList_s1bDES)

    output_directory = os.path.join(S1chain.outputPreProcess, tile)
    if not os.path.exists(output_directory):
        try:
            os.mkdir(output_directory)
        except:
            print("{} already exists".format(output_directory))
    launch_sar_reprojection_prod = partial(launch_sar_reprojection,
                                           refRaster=refRaster,
                                           tileName=tile,
                                           geoid=S1chain.geoid,
                                           SRTM=S1chain.SRTM,
                                           output_directory=output_directory,
                                           RAMPerProcess=RAMPerProcess,
                                           workingDirectory=workingDirectory)

    rasterList_s1aASC_reproj = []
    for elem in rasterList_s1aASC:
        rasterList_s1aASC_reproj.append(launch_sar_reprojection_prod(elem))

    rasterList_s1aDES_reproj = []
    for elem in rasterList_s1aDES:
        rasterList_s1aDES_reproj.append(launch_sar_reprojection_prod(elem))

    rasterList_s1bASC_reproj = []
    for elem in rasterList_s1bASC:
        rasterList_s1bASC_reproj.append(launch_sar_reprojection_prod(elem))

    rasterList_s1bDES_reproj = []
    for elem in rasterList_s1bDES:
        rasterList_s1bDES_reproj.append(launch_sar_reprojection_prod(elem))

    rasterList_s1aASC_reproj_flat = [
        pol for SAR_date in rasterList_s1aASC_reproj for pol in SAR_date
    ]
    rasterList_s1aDES_reproj_flat = [
        pol for SAR_date in rasterList_s1aDES_reproj for pol in SAR_date
    ]
    rasterList_s1bASC_reproj_flat = [
        pol for SAR_date in rasterList_s1bASC_reproj for pol in SAR_date
    ]
    rasterList_s1bDES_reproj_flat = [
        pol for SAR_date in rasterList_s1bDES_reproj for pol in SAR_date
    ]

    allOrtho_path = rasterList_s1aASC_reproj_flat + rasterList_s1aDES_reproj_flat + rasterList_s1bASC_reproj_flat + rasterList_s1bDES_reproj_flat

    s1aASC_masks = [
        s1aASC.replace(".tif", "_BorderMask.tif")
        for s1aASC in rasterList_s1aASC_reproj_flat if "_vv_" in s1aASC
    ]
    s1aDES_masks = [
        s1aDES.replace(".tif", "_BorderMask.tif")
        for s1aDES in rasterList_s1aDES_reproj_flat if "_vv_" in s1aDES
    ]
    s1bASC_masks = [
        s1bASC.replace(".tif", "_BorderMask.tif")
        for s1bASC in rasterList_s1bASC_reproj_flat if "_vv_" in s1bASC
    ]
    s1bDES_masks = [
        s1bDES.replace(".tif", "_BorderMask.tif")
        for s1bDES in rasterList_s1bDES_reproj_flat if "_vv_" in s1bDES
    ]
    allMasks = s1aASC_masks + s1aDES_masks + s1bASC_masks + s1bDES_masks

    date_tile = {'s1_ASC': s1_ASC_dates, 's1_DES': s1_DES_dates}

    #sort detected dates
    for k, v in list(date_tile.items()):
        v.sort()

    #launch outcore generation and prepare mulitemporal filtering
    filtered = S1FilteringProcessor.main(allOrtho_path, cfg, date_tile, tile)
    allFiltered = []
    allMasksOut = []

    for S1_filtered, a, b in filtered:
        if convert_to_interger:
            S1_filtered.Execute()
            convert = SAR_floatToInt(S1_filtered,
                                     comp_per_date * len(date_tile[mode]),
                                     RAMPerProcess)
            allFiltered.append(convert)
        else:
            allFiltered.append(S1_filtered)

    allMasksOut.append(allMasks)

    #In order to avoid "TypeError: can't pickle SwigPyObject objects"
    if getFiltered:
        return allFiltered, allMasksOut
