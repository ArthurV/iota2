#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import datetime
import logging
import os

LOGGER = logging.getLogger("distributed.worker")


def CreateFichierDatesReg(start_date: str, end_date: str, gap_days: int,
                          out_path: str, sensorName: str):
    """
    start : str = YYYYMMDD
    end : str = YYYYMMDD
    gap : time between two images in days
    """
    date_init = datetime.date(int(start_date[0:4]), int(start_date[4:6]),
                              int(start_date[6:8]))
    date_end = datetime.date(int(end_date[0:4]), int(end_date[4:6]),
                             int(end_date[6:8]))

    outputDateFile = out_path + "/DatesInterpReg" + sensorName + ".txt"
    if not os.path.exists(outputDateFile):
        fich = open(outputDateFile, "w")
        gap = int(gap_days)
        ndate = date_init.isoformat()
        ndate = ndate.split("-")
        ndate = ndate[0] + ndate[1] + ndate[2]

        fich.write(ndate + "\n")

        date = date_init + datetime.timedelta(days=gap)
        date = date_init
        date_end_1 = date_end - datetime.timedelta(days=1)
        while (date + datetime.timedelta(days=gap) < date_end_1):
            new_date = date + datetime.timedelta(days=gap)

            ndate = new_date.isoformat()
            ndate = ndate.split("-")
            ndate = ndate[0] + ndate[1] + ndate[2]
            date = new_date
            fich.write(ndate + "\n")

        ndate = date_end.isoformat()
        ndate = ndate.split("-")
        ndate = ndate[0] + ndate[1] + ndate[2]
        fich.write(ndate)
        fich.close()
    return out_path + "/DatesInterpReg" + sensorName + ".txt"
