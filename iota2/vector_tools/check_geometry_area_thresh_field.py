#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import sys

from iota2.common.utils import run
from iota2.vector_tools import (add_field_area, add_field_id,
                                delete_duplicate_geometries_sqlite,
                                delete_field, multipoly_to_poly,
                                select_by_size)
from iota2.vector_tools import vector_functions as vf


def check_geometry_area_thresh_field(shapefile,
                                     pixelArea,
                                     pix_thresh,
                                     outshape="",
                                     outformat="ESRI Shapefile"):

    tmpfile = []

    if outshape == "":
        outshape = shapefile

    if os.path.splitext(shapefile)[1] == ".shp":
        informat = "ESRI shapefile"
    elif os.path.splitext(shapefile)[1] == ".sqlite":
        informat = "SQLite"
    else:
        print("Input format not managed")
        sys.exit()

    if os.path.splitext(outshape)[1] == ".shp":
        outformat = "ESRI shapefile"
    elif os.path.splitext(outshape)[1] == ".sqlite":
        outformat = "SQLite"
    else:
        print("Output format not managed")
        sys.exit()

    # Empty geometry identification
    try:
        outShapefileGeom, _ = vf.checkEmptyGeom(shapefile,
                                                informat,
                                                output_file=shapefile)
        if shapefile != outshape:
            tmpfile.append(outShapefileGeom)

        print('Check empty geometries succeeded')

    except Exception as e:
        print('Check empty geometries did not work for the following error :')
        print(e)

    # suppression des doubles géométries
    delete_duplicate_geometries_sqlite.deleteDuplicateGeometriesSqlite(
        outShapefileGeom, output_file=outShapefileGeom)

    # Suppression des multipolygons
    shapefileNoDupspoly = os.path.splitext(
        outShapefileGeom)[0] + 'spoly' + '.shp'
    tmpfile.append(shapefileNoDupspoly)
    driv = vf.getDriver(outShapefileGeom)
    try:
        multipoly_to_poly.multipoly2poly(outShapefileGeom,
                                         shapefileNoDupspoly,
                                         outformat=driv)
        print(
            'Conversion of multipolygons shapefile to single polygons succeeded'
        )
    except Exception as e:
        print(
            'Conversion of multipolygons shapefile to single polygons did not work for the following error :'
        )
        print(e)

    # recompute areas
    try:
        add_field_area.addFieldArea(shapefileNoDupspoly, pixelArea)
    except Exception as e:
        print('Add an Area field did not work for the following error :')
        print(e)

    # Attribution d'un ID
    driv = vf.getDriver(shapefileNoDupspoly)
    fieldList = vf.getFields(shapefileNoDupspoly, driv)
    if 'ID' in fieldList:
        delete_field.deleteField(shapefileNoDupspoly, 'ID')
        add_field_id.addFieldID(shapefileNoDupspoly)
    else:
        add_field_id.addFieldID(shapefileNoDupspoly)

    # Filter by Area
    try:
        select_by_size.selectBySize(shapefileNoDupspoly, 'Area', pix_thresh,
                                    outshape)
        print(
            'Selection by size upper {} pixel(s) succeeded'.format(pix_thresh))
    except Exception as e:
        print('Selection by size did not work for the following error :')
        print(e)
    if int(pix_thresh) > 0:
        try:
            select_by_size.selectBySize(shapefileNoDupspoly, 'Area',
                                        pix_thresh, outshape)
            print('Selection by size upper {} pixel(s) succeeded'.format(
                pix_thresh))
        except Exception as e:
            print('Selection by size did not work for the following error :')
            print(e)
    elif int(pix_thresh) < 0:
        print("Area threshold has to be positive !")
        sys.exit()

    # Check geometry
    vf.checkValidGeom(outshape, outformat)

    # delete tmp file
    for fileDel in tmpfile:
        basefile = os.path.splitext(fileDel)[0]
        run('rm {}.*'.format(basefile))


if __name__ == "__main__":
    if len(sys.argv) == 1:
        prog = os.path.basename(sys.argv[0])
        print('      ' + sys.argv[0] + ' [options]')
        print("     Help : ", prog, " --help")
        print("        or : ", prog, " -h")
        sys.exit(-1)
    else:
        usage = "usage: %prog [options] "
        parser = argparse.ArgumentParser(description = "Manage shapefile : " \
        "1. Check geometry, "
        "2. Delete Duplicate geometries, "
        "3. Calulate Area, "
        "4. Harmonize ID field, "
        "5. Delete MultiPolygons")
        parser.add_argument("-s", dest="shapefile", action="store", \
                            help="Input shapefile", required = True)
        parser.add_argument("-p", dest="pixelSize", action="store", \
                            help="Pixel size", required = True)
        parser.add_argument("-at", dest="area", action="store", \
                            help="Area threshold in pixel unit", required = True)
        parser.add_argument("-o", dest="outpath", action="store", \
                            help="ESRI Shapefile output filename and path", required = True)
        args = parser.parse_args()

        check_geometry_area_thresh_field(args.shapefile, args.pixelSize,
                                         args.area, args.outpath)
