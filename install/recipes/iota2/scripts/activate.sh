#!/bin/bash

# Store existing env vars and set to this conda env

# if [[ -n "${PATH}" ]]; then
#     export _CONDA_SET_PATH=${PATH}
# fi

# if [ -d ${CONDA_PREFIX}/lib/python3.6/site-packages/iota2 ]; then
#     export PATH=${CONDA_PREFIX}/lib/python3.6/site-packages/iota2:${CONDA_PREFIX}/lib/python3.6/site-packages/iota2/common/tools:${_CONDA_SET_PATH}
#     chmod uo+x ${CONDA_PREFIX}/lib/python3.6/site-packages/iota2/Iota2.py
#     chmod uo+x ${CONDA_PREFIX}/lib/python3.6/site-packages/iota2/Iota2Cluster.py
#     chmod uo+x ${CONDA_PREFIX}/lib/python3.6/site-packages/iota2/common/tools/check_database.py
#     chmod uo+x ${CONDA_PREFIX}/lib/python3.6/site-packages/iota2/common/tools/check_region_database.py
#     chmod uo+x ${CONDA_PREFIX}/lib/python3.6/site-packages/iota2/common/tools/concat_sample_selections.py
# fi
