#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module containing all natively usable pytorch neural networks in iota2"""

from typing import Dict, List, Optional

import torch
import torch.nn as nn
import torch.nn.functional as F

from iota2.learning.pytorch.torch_nn_bank import Iota2NeuralNetwork


class ANN_fail_inheritance(nn.Module):
    """Same as ANN but reshape it's inputs
    """
    def __init__(self,
                 nb_features: int,
                 nb_class: int,
                 layer: int = 1,
                 doy_sensors_dic: Optional[Dict] = None,
                 **kwargs):
        super().__init__(nb_features, nb_class, doy_sensors_dic)
        self.nb_class = nb_class
        self.layer = layer
        self.sensors_doy = doy_sensors_dic
        self.fc1 = nn.Linear(in_features=nb_features, out_features=16)
        self.fc2 = nn.Linear(in_features=16, out_features=12)
        self.output = nn.Linear(in_features=12, out_features=nb_class)

    def forward(self, s2_theia):
        # flat x due to flat input nn first layer
        x = s2_theia.reshape(-1, s2_theia.shape[1] * s2_theia.shape[-1])
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = torch.softmax(self.output(x), dim=1)
        return x
