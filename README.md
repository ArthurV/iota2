<img src="assets/iota2-logo.png" width="100">

Infrastructure pour l'Occupation des sols par Traitement Automatique Incorporant les Orfeo Toolbox Applications - iota<sup>2</sup> 

Getting started with iota2? Look at the online documentation using one of the following links:

- http://osr-cesbio.ups-tlse.fr/oso/donneeswww_TheiaOSO/iota2_documentation/master/index.html offical link (currently broken)
- http://lannister.ups-tlse.fr/oso/donneeswww_TheiaOSO/iota2_documentation/develop/index.html official link (currently active)
- https://link.infini.fr/iota2 short URL pointing to the first broken official link
- https://link.infini.fr/iota2-2 short link pointing to the second official link

[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)
