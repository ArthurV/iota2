Group init:
	 [x] Step 1: check inputs
	 [x] Step 2: Sensors pre-processing
	 [x] Step 3: Generate a common masks for each sensors
	 [x] Step 4: Compute validity raster by tile
	 [x] Step 5: Compute SLIC segmentation by tile
	 [x] Step 6: Vectorize segmentation by tiles
	 [x] Step 7: Compute intersection between segment and region
Group sampling:
	 [x] Step 8: Generate tile's envelope
	 [x] Step 9: Generate a region vector
	 [x] Step 10: Prepare samples
	 [x] Step 11: merge samples by models
	 [x] Step 12: Intersect samples and segmentation
	 [x] Step 13: Compute zonal statistics for learning polygons
Group learning:
	 [x] Step 14: learn model for obia
Group classification:
	 [x] Step 15: Classify tiles using zonal stats
Group validation:
	 [x] Step 16: Reassemble tiles and compute confusion matrix
	 [x] Step 17: Merge all vector tiles
	 [x] Step 18: Merge all metrics files

