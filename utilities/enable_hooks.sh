#!/usr/bin/env python3

import os

src_dir = os.path.dirname(os.path.realpath(__file__))

# commit hook
commit_hook = os.path.join(src_dir, "..", ".git", "hooks", "pre-commit")
if os.path.islink(commit_hook):
    os.unlink(commit_hook)
os.symlink(os.path.join(src_dir, "pre-commit"), commit_hook)

# push hook
push_hook = os.path.join(src_dir, "..", ".git", "hooks", "pre-push")
if os.path.islink(push_hook):
    os.unlink(push_hook)
os.symlink(os.path.join(src_dir, "pre-push"), push_hook)
